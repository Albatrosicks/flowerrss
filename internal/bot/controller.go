package bot

import (
	"bytes"
	"fmt"
	"html/template"
	"strconv"
	"strings"
	"time"

	"andhrimnir.albi.io/albi/flowerss-bot/internal/bot/fsm"
	"andhrimnir.albi.io/albi/flowerss-bot/internal/config"
	"andhrimnir.albi.io/albi/flowerss-bot/internal/model"

	"go.uber.org/zap"
	tb "gopkg.in/tucnak/telebot.v2"
)

var (
	feedSettingTmpl = `
订阅<b>设置</b>
[id] {{ .sub.ID }}
[标题] {{ .source.Title }}
[Link] {{.source.Link }}
[抓取更新] {{if ge .source.ErrorCount .Count }}暂停{{else if lt .source.ErrorCount .Count }}抓取中{{end}}
[抓取频率] {{ .sub.Interval }}分钟
[通知] {{if eq .sub.EnableNotification 0}}关闭{{else if eq .sub.EnableNotification 1}}开启{{end}}
[Telegraph] {{if eq .sub.EnableTelegraph 0}}关闭{{else if eq .sub.EnableTelegraph 1}}开启{{end}}
[Tag] {{if .sub.Tag}}{{ .sub.Tag }}{{else}}无{{end}}
`
)

func toggleCtrlButtons(c *tb.Callback, action string) {

	if (c.Message.Chat.Type == tb.ChatGroup || c.Message.Chat.Type == tb.ChatSuperGroup) &&
		!userIsAdminOfGroup(c.Sender.ID, c.Message.Chat) {
		// check admin
		return
	}

	data := strings.Split(c.Data, ":")
	subscriberID, _ := strconv.Atoi(data[0])
	// 如果订阅者与按钮点击者id不一致，需要验证管理员权限
	if subscriberID != c.Sender.ID {
		channelChat, err := B.ChatByID(fmt.Sprintf("%d", subscriberID))

		if err != nil {
			return
		}

		if !UserIsAdminChannel(c.Sender.ID, channelChat) {
			return
		}
	}

	msg := strings.Split(c.Message.Text, "\n")
	subID, err := strconv.Atoi(strings.Split(msg[1], " ")[1])
	if err != nil {
		_ = B.Respond(c, &tb.CallbackResponse{
			Text: "error",
		})
		return
	}
	sub, err := model.GetSubscribeByID(subID)
	if sub == nil || err != nil {
		_ = B.Respond(c, &tb.CallbackResponse{
			Text: "error",
		})
		return
	}

	source, _ := model.GetSourceById(sub.SourceID)
	t := template.New("setting template")
	_, _ = t.Parse(feedSettingTmpl)

	switch action {
	case "toggleNotice":
		err = sub.ToggleNotification()
	case "toggleTelegraph":
		err = sub.ToggleTelegraph()
	case "toggleUpdate":
		err = source.ToggleEnabled()
	}

	if err != nil {
		_ = B.Respond(c, &tb.CallbackResponse{
			Text: "error",
		})
		return
	}

	sub.Save()

	text := new(bytes.Buffer)

	_ = t.Execute(text, map[string]interface{}{"source": source, "sub": sub, "Count": config.ErrorThreshold})
	_ = B.Respond(c, &tb.CallbackResponse{
		Text: "修改成功",
	})
	_, _ = B.Edit(c.Message, text.String(), &tb.SendOptions{
		ParseMode: tb.ModeHTML,
	}, &tb.ReplyMarkup{
		InlineKeyboard: genFeedSetBtn(c, sub, source),
	})
}

func startCmdCtr(m *tb.Message) {
	user, _ := model.FindOrCreateUserByTelegramID(m.Chat.ID)
	zap.S().Infof("/start user_id: %d telegram_id: %d", user.ID, user.TelegramID)
	_, _ = B.Send(m.Chat, fmt.Sprintf("Ку, приветствую тебя в flowerss."))
}

func subCmdCtr(m *tb.Message) {

	url, mention := GetURLAndMentionFromMessage(m)

	if mention == "" {
		if url != "" {
			registFeed(m.Chat, url)
		} else {
			_, err := B.Send(m.Chat, "Ответь реплаем с ссылкой на RSS", &tb.ReplyMarkup{ForceReply: true})
			if err == nil {
				UserState[m.Chat.ID] = fsm.Sub
			}
		}
	} else {
		if url != "" {
			FeedForChannelRegister(m, url, mention)
		} else {
			_, _ = B.Send(m.Chat, "Чтобы подписаться на канал, используй команду '/sub @ChannelID URL'")
		}
	}

}

func exportCmdCtr(m *tb.Message) {

	mention := GetMentionFromMessage(m)
	var sourceList []model.Source
	var err error
	if mention == "" {

		sourceList, err = model.GetSourcesByUserID(m.Chat.ID)
		if err != nil {
			zap.S().Warnf(err.Error())
			_, _ = B.Send(m.Chat, fmt.Sprintf("Ошибка экспорта"))
			return
		}
	} else {
		channelChat, err := B.ChatByID(mention)

		if err != nil {
			_, _ = B.Send(m.Chat, "error")
			return
		}

		adminList, err := B.AdminsOf(channelChat)
		if err != nil {
			_, _ = B.Send(m.Chat, "error")
			return
		}

		senderIsAdmin := false
		for _, admin := range adminList {
			if m.Sender.ID == admin.User.ID {
				senderIsAdmin = true
			}
		}

		if !senderIsAdmin {
			_, _ = B.Send(m.Chat, fmt.Sprintf("Не администратор канала не может выполнить эту операцию"))
			return
		}

		sourceList, err = model.GetSourcesByUserID(channelChat.ID)
		if err != nil {
			zap.S().Errorf(err.Error())
			_, _ = B.Send(m.Chat, fmt.Sprintf("Ошибка экспорта"))
			return
		}
	}

	if len(sourceList) == 0 {
		_, _ = B.Send(m.Chat, fmt.Sprintf("Список подписок пуст"))
		return
	}

	opmlStr, err := ToOPML(sourceList)

	if err != nil {
		_, _ = B.Send(m.Chat, fmt.Sprintf("Ошибка экспорта"))
		return
	}
	opmlFile := &tb.Document{File: tb.FromReader(strings.NewReader(opmlStr))}
	opmlFile.FileName = fmt.Sprintf("subscriptions_%d.opml", time.Now().Unix())
	_, err = B.Send(m.Chat, opmlFile)

	if err != nil {
		_, _ = B.Send(m.Chat, fmt.Sprintf("Ошибка экспорта"))
		zap.S().Errorf("send opml file failed, err:%+v", err)
	}

}

func listCmdCtr(m *tb.Message) {
	mention := GetMentionFromMessage(m)

	var rspMessage string
	if mention != "" {
		// channel fetcher list
		channelChat, err := B.ChatByID(mention)
		if err != nil {
			_, _ = B.Send(m.Chat, "error")
			return
		}

		if !checkPermitOfChat(int64(m.Sender.ID), channelChat) {
			B.Send(m.Chat, fmt.Sprintf("Не администратор канала не может выполнить эту операцию"))
			return
		}

		user, err := model.FindOrCreateUserByTelegramID(channelChat.ID)
		if err != nil {
			B.Send(m.Chat, fmt.Sprintf("Внутренняя ошибка list@1"))
			return
		}

		subSourceMap, err := user.GetSubSourceMap()
		if err != nil {
			B.Send(m.Chat, fmt.Sprintf("Внутренняя ошибка list@2"))
			return
		}

		sources, _ := model.GetSourcesByUserID(channelChat.ID)
		rspMessage = fmt.Sprintf("Канал [%s](https://t.me/%s) Список подписок：\n", channelChat.Title, channelChat.Username)
		if len(sources) == 0 {
			rspMessage = fmt.Sprintf("频道 [%s](https://t.me/%s) Список подписок пуст", channelChat.Title, channelChat.Username)
		} else {
			for sub, source := range subSourceMap {
				rspMessage = rspMessage + fmt.Sprintf("[[%d]] [%s](%s)\n", sub.ID, source.Title, source.Link)
			}
		}
	} else {
		// private chat or group
		if m.Chat.Type != tb.ChatPrivate && !checkPermitOfChat(int64(m.Sender.ID), m.Chat) {
			// 无权限
			return
		}

		user, err := model.FindOrCreateUserByTelegramID(m.Chat.ID)
		if err != nil {
			B.Send(m.Chat, fmt.Sprintf("Внутренняя ошибка list@1"))
			return
		}

		subSourceMap, err := user.GetSubSourceMap()
		if err != nil {
			B.Send(m.Chat, fmt.Sprintf("Внутренняя ошибка list@2"))
			return
		}

		rspMessage = "Текущий список подписок: \n"
		if len(subSourceMap) == 0 {
			rspMessage = "Список подписок пуст"
		} else {
			for sub, source := range subSourceMap {
				rspMessage = rspMessage + fmt.Sprintf("[[%d]] [%s](%s)\n", sub.ID, source.Title, source.Link)
			}
		}
	}
	_, _ = B.Send(m.Chat, rspMessage, &tb.SendOptions{
		DisableWebPagePreview: true,
		ParseMode:             tb.ModeMarkdown,
	})
}

func checkCmdCtr(m *tb.Message) {
	mention := GetMentionFromMessage(m)
	if mention != "" {
		channelChat, err := B.ChatByID(mention)
		if err != nil {
			_, _ = B.Send(m.Chat, "error")
			return
		}
		adminList, err := B.AdminsOf(channelChat)
		if err != nil {
			_, _ = B.Send(m.Chat, "error")
			return
		}

		senderIsAdmin := false
		for _, admin := range adminList {
			if m.Sender.ID == admin.User.ID {
				senderIsAdmin = true
			}
		}

		if !senderIsAdmin {
			_, _ = B.Send(m.Chat, fmt.Sprintf("Не администратор канала не может выполнить эту операцию"))
			return
		}

		sources, _ := model.GetErrorSourcesByUserID(channelChat.ID)
		message := fmt.Sprintf("Канал [% s] (https://t.me/%s) список истекших подписок: \n", channelChat.Title, channelChat.Username)
		if len(sources) == 0 {
			message = fmt.Sprintf("Канал [% s] (https://t.me/%s) Все подписки нормальные", channelChat.Title, channelChat.Username)
		} else {
			for _, source := range sources {
				message = message + fmt.Sprintf("[[%d]] [%s](%s)\n", source.ID, source.Title, source.Link)
			}
		}

		_, _ = B.Send(m.Chat, message, &tb.SendOptions{
			DisableWebPagePreview: true,
			ParseMode:             tb.ModeMarkdown,
		})

	} else {
		sources, _ := model.GetErrorSourcesByUserID(m.Chat.ID)
		message := "Список истекших подписок: \n"
		if len(sources) == 0 {
			message = "Все подписки нормальные"
		} else {
			for _, source := range sources {
				message = message + fmt.Sprintf("[[%d]] [%s](%s)\n", source.ID, source.Title, source.Link)
			}
		}
		_, _ = B.Send(m.Chat, message, &tb.SendOptions{
			DisableWebPagePreview: true,
			ParseMode:             tb.ModeMarkdown,
		})
	}

}

func setCmdCtr(m *tb.Message) {

	mention := GetMentionFromMessage(m)
	var sources []model.Source
	var ownerID int64
	// 获取订阅列表
	if mention == "" {
		sources, _ = model.GetSourcesByUserID(m.Chat.ID)
		ownerID = int64(m.Chat.ID)
		if len(sources) <= 0 {
			_, _ = B.Send(m.Chat, "В настоящее время нет каналов")
			return
		}

	} else {

		channelChat, err := B.ChatByID(mention)

		if err != nil {
			_, _ = B.Send(m.Chat, "Ошибка при получении информации о канале.")
			return
		}

		if UserIsAdminChannel(m.Sender.ID, channelChat) {
			sources, _ = model.GetSourcesByUserID(channelChat.ID)

			if len(sources) <= 0 {
				_, _ = B.Send(m.Chat, "На канале нет фидов.")
				return
			}
			ownerID = channelChat.ID

		} else {
			_, _ = B.Send(m.Chat, "Администраторы без каналов не могут выполнить эту операцию.")
			return
		}

	}

	var replyButton []tb.ReplyButton
	replyKeys := [][]tb.ReplyButton{}
	setFeedItemBtns := [][]tb.InlineButton{}

	// 配置按钮
	for _, source := range sources {
		// 添加按钮
		text := fmt.Sprintf("%s %s", source.Title, source.Link)
		replyButton = []tb.ReplyButton{
			tb.ReplyButton{Text: text},
		}
		replyKeys = append(replyKeys, replyButton)

		setFeedItemBtns = append(setFeedItemBtns, []tb.InlineButton{
			tb.InlineButton{
				Unique: "set_feed_item_btn",
				Text:   fmt.Sprintf("[%d] %s", source.ID, source.Title),
				Data:   fmt.Sprintf("%d:%d", ownerID, source.ID),
			},
		})
	}

	_, _ = B.Send(m.Chat, "Пожалуйста, выберите источник, который вы хотите установить", &tb.ReplyMarkup{
		InlineKeyboard: setFeedItemBtns,
	})
}

func setFeedItemBtnCtr(c *tb.Callback) {

	if (c.Message.Chat.Type == tb.ChatGroup || c.Message.Chat.Type == tb.ChatSuperGroup) &&
		!userIsAdminOfGroup(c.Sender.ID, c.Message.Chat) {
		return
	}

	data := strings.Split(c.Data, ":")
	subscriberID, _ := strconv.Atoi(data[0])

	// 如果订阅者与按钮点击者id不一致，需要验证管理员权限

	if subscriberID != c.Sender.ID {
		channelChat, err := B.ChatByID(fmt.Sprintf("%d", subscriberID))

		if err != nil {
			return
		}

		if !UserIsAdminChannel(c.Sender.ID, channelChat) {
			return
		}
	}

	sourceID, _ := strconv.Atoi(data[1])

	source, err := model.GetSourceById(uint(sourceID))

	if err != nil {
		_, _ = B.Edit(c.Message, "Канал не найден, код ошибки 01.")
		return
	}

	sub, err := model.GetSubscribeByUserIDAndSourceID(int64(subscriberID), source.ID)
	if err != nil {
		_, _ = B.Edit(c.Message, "Пользователь не подписался на rss, код ошибки 02.")
		return
	}

	t := template.New("setting template")
	_, _ = t.Parse(feedSettingTmpl)
	text := new(bytes.Buffer)
	_ = t.Execute(text, map[string]interface{}{"source": source, "sub": sub, "Count": config.ErrorThreshold})

	_, _ = B.Edit(
		c.Message,
		text.String(),
		&tb.SendOptions{
			ParseMode: tb.ModeHTML,
		}, &tb.ReplyMarkup{
			InlineKeyboard: genFeedSetBtn(c, sub, source),
		},
	)
}

func setSubTagBtnCtr(c *tb.Callback) {

	// 权限验证
	if !feedSetAuth(c) {
		return
	}
	data := strings.Split(c.Data, ":")
	ownID, _ := strconv.Atoi(data[0])
	sourceID, _ := strconv.Atoi(data[1])

	sub, err := model.GetSubscribeByUserIDAndSourceID(int64(ownID), uint(sourceID))
	if err != nil {
		_, _ = B.Send(
			c.Message.Chat,
			"Системная ошибка, код 04",
		)
		return
	}
	msg := fmt.Sprintf(
		"Для установки тегов для этой подписки используйте команду `/setfeedtag %d tags`. Теги - это теги, которые необходимо установить, разделенные пробелами. (Можно установить до трех меток) \n"+
			"Например: `/setfeedtag %d technology apple`",
		sub.ID, sub.ID)

	_ = B.Delete(c.Message)

	_, _ = B.Send(
		c.Message.Chat,
		msg,
		&tb.SendOptions{ParseMode: tb.ModeMarkdown},
	)
}

func genFeedSetBtn(c *tb.Callback, sub *model.Subscribe, source *model.Source) [][]tb.InlineButton {
	setSubTagKey := tb.InlineButton{
		Unique: "set_set_sub_tag_btn",
		Text:   "Настройки ярлыка",
		Data:   c.Data,
	}

	toggleNoticeKey := tb.InlineButton{
		Unique: "set_toggle_notice_btn",
		Text:   "Включите уведомления",
		Data:   c.Data,
	}
	if sub.EnableNotification == 1 {
		toggleNoticeKey.Text = "Закрыть уведомление"
	}

	toggleTelegraphKey := tb.InlineButton{
		Unique: "set_toggle_telegraph_btn",
		Text:   "Включите транскодирование Telegraph",
		Data:   c.Data,
	}
	if sub.EnableTelegraph == 1 {
		toggleTelegraphKey.Text = "Отключить перекодировку Telegraph"
	}

	toggleEnabledKey := tb.InlineButton{
		Unique: "set_toggle_update_btn",
		Text:   "Приостановить обновление",
		Data:   c.Data,
	}

	if source.ErrorCount >= config.ErrorThreshold {
		toggleEnabledKey.Text = "Перезапустить обновление"
	}

	feedSettingKeys := [][]tb.InlineButton{
		[]tb.InlineButton{
			toggleEnabledKey,
			toggleNoticeKey,
		},
		[]tb.InlineButton{
			toggleTelegraphKey,
			setSubTagKey,
		},
	}
	return feedSettingKeys
}

func setToggleNoticeBtnCtr(c *tb.Callback) {
	toggleCtrlButtons(c, "toggleNotice")
}

func setToggleTelegraphBtnCtr(c *tb.Callback) {
	toggleCtrlButtons(c, "toggleTelegraph")
}

func setToggleUpdateBtnCtr(c *tb.Callback) {
	toggleCtrlButtons(c, "toggleUpdate")
}

func unsubCmdCtr(m *tb.Message) {

	url, mention := GetURLAndMentionFromMessage(m)

	if mention == "" {
		if url != "" {
			//Unsub by url
			source, _ := model.GetSourceByUrl(url)
			if source == nil {
				_, _ = B.Send(m.Chat, "Не подписан на этот RSS-канал")
			} else {
				err := model.UnsubByUserIDAndSource(m.Chat.ID, source)
				if err == nil {
					_, _ = B.Send(
						m.Chat,
						fmt.Sprintf("[%s] (%s) Подписка успешно отменена!", source.Title, source.Link),
						&tb.SendOptions{
							DisableWebPagePreview: true,
							ParseMode:             tb.ModeMarkdown,
						},
					)
					zap.S().Infof("%d unsubscribe [%d]%s %s", m.Chat.ID, source.ID, source.Title, source.Link)
				} else {
					_, err = B.Send(m.Chat, err.Error())
				}
			}
		} else {
			//Unsub by button

			subs, err := model.GetSubsByUserID(m.Chat.ID)

			if err != nil {
				errorCtr(m, "Ошибка бота, обратитесь к администратору! Код ошибки 01")
				return
			}

			if len(subs) > 0 {
				unsubFeedItemBtns := [][]tb.InlineButton{}

				for _, sub := range subs {

					source, err := model.GetSourceById(sub.SourceID)
					if err != nil {
						errorCtr(m, "Ошибка бота, обратитесь к администратору! Код ошибки 02")
						return
					}

					unsubFeedItemBtns = append(unsubFeedItemBtns, []tb.InlineButton{
						tb.InlineButton{
							Unique: "unsub_feed_item_btn",
							Text:   fmt.Sprintf("[%d] %s", sub.SourceID, source.Title),
							Data:   fmt.Sprintf("%d:%d:%d", sub.UserID, sub.ID, source.ID),
						},
					})
				}

				_, _ = B.Send(m.Chat, "Пожалуйста, выберите источник, от подписки которого вы хотите отказаться", &tb.ReplyMarkup{
					InlineKeyboard: unsubFeedItemBtns,
				})
			} else {
				_, _ = B.Send(m.Chat, "В настоящее время нет каналов")
			}
		}
	} else {
		if url != "" {
			channelChat, err := B.ChatByID(mention)
			if err != nil {
				_, _ = B.Send(m.Chat, "error")
				return
			}
			adminList, err := B.AdminsOf(channelChat)
			if err != nil {
				_, _ = B.Send(m.Chat, "error")
				return
			}

			senderIsAdmin := false
			for _, admin := range adminList {
				if m.Sender.ID == admin.User.ID {
					senderIsAdmin = true
				}
			}

			if !senderIsAdmin {
				_, _ = B.Send(m.Chat, fmt.Sprintf("Не администратор канала не может выполнить эту операцию"))
				return
			}

			source, _ := model.GetSourceByUrl(url)
			sub, err := model.GetSubByUserIDAndURL(channelChat.ID, url)

			if err != nil {
				if err.Error() == "record not found" {
					_, _ = B.Send(
						m.Chat,
						fmt.Sprintf("频道 [%s](https://t.me/%s) Не подписан на этот RSS-канал", channelChat.Title, channelChat.Username),
						&tb.SendOptions{
							DisableWebPagePreview: true,
							ParseMode:             tb.ModeMarkdown,
						},
					)

				} else {
					_, _ = B.Send(m.Chat, "Отписаться не удалось")
				}
				return

			}

			err = sub.Unsub()
			if err == nil {
				_, _ = B.Send(
					m.Chat,
					fmt.Sprintf("Подписка на канал [%s] (https://t.me/%s) отменена [%s] (%s) успешно завершена", channelChat.Title, channelChat.Username, source.Title, source.Link),
					&tb.SendOptions{
						DisableWebPagePreview: true,
						ParseMode:             tb.ModeMarkdown,
					},
				)
				zap.S().Infof("%d for [%d]%s unsubscribe %s", m.Chat.ID, source.ID, source.Title, source.Link)
			} else {
				_, err = B.Send(m.Chat, err.Error())
			}
			return

		}
		_, _ = B.Send(m.Chat, "Чтобы отказаться от подписки на канал, используйте команду '/unsub @ChannelID URL'")
	}

}

func unsubFeedItemBtnCtr(c *tb.Callback) {

	if (c.Message.Chat.Type == tb.ChatGroup || c.Message.Chat.Type == tb.ChatSuperGroup) &&
		!userIsAdminOfGroup(c.Sender.ID, c.Message.Chat) {
		// check admin
		return
	}

	data := strings.Split(c.Data, ":")
	if len(data) == 3 {
		userID, _ := strconv.Atoi(data[0])
		subID, _ := strconv.Atoi(data[1])
		sourceID, _ := strconv.Atoi(data[2])
		source, _ := model.GetSourceById(uint(sourceID))

		rtnMsg := fmt.Sprintf("[%d] <a href=\"%s\">%s </a> успешно отменили подписку", sourceID, source.Link, source.Title)

		err := model.UnsubByUserIDAndSubID(int64(userID), uint(subID))

		if err == nil {
			_, _ = B.Edit(
				c.Message,
				rtnMsg,
				&tb.SendOptions{
					ParseMode: tb.ModeHTML,
				},
			)
			return
		}
	}
	_, _ = B.Edit(c.Message, "Ошибка отказа от подписки!")
}

func unsubAllCmdCtr(m *tb.Message) {
	mention := GetMentionFromMessage(m)
	confirmKeys := [][]tb.InlineButton{}
	confirmKeys = append(confirmKeys, []tb.InlineButton{
		tb.InlineButton{
			Unique: "unsub_all_confirm_btn",
			Text:   "подтверждать",
		},
		tb.InlineButton{
			Unique: "unsub_all_cancel_btn",
			Text:   "Отмена",
		},
	})

	var msg string

	if mention == "" {
		msg = "Вы хотите отказаться от подписки на все подписки текущего пользователя?"
	} else {
		msg = fmt.Sprintf("Вы хотите, чтобы %s отписался от всех подписок на этот канал?", mention)
	}

	_, _ = B.Send(
		m.Chat,
		msg,
		&tb.SendOptions{
			ParseMode: tb.ModeHTML,
		}, &tb.ReplyMarkup{
			InlineKeyboard: confirmKeys,
		},
	)
}

func unsubAllCancelBtnCtr(c *tb.Callback) {
	_, _ = B.Edit(c.Message, "操作Отмена")
}

func unsubAllConfirmBtnCtr(c *tb.Callback) {
	mention := GetMentionFromMessage(c.Message)
	var msg string
	if mention == "" {
		success, fail, err := model.UnsubAllByUserID(int64(c.Sender.ID))
		if err != nil {
			msg = "Отписаться не удалось"
		} else {
			msg = fmt.Sprintf("退订成功：%d\nОтписаться не удалось：%d", success, fail)
		}

	} else {
		channelChat, err := B.ChatByID(mention)

		if err != nil {
			_, _ = B.Edit(c.Message, "error")
			return
		}

		if UserIsAdminChannel(c.Sender.ID, channelChat) {
			success, fail, err := model.UnsubAllByUserID(channelChat.ID)
			if err != nil {
				msg = "Отписаться не удалось"

			} else {
				msg = fmt.Sprintf("退订成功：%d\nОтписаться не удалось：%d", success, fail)
			}

		} else {
			msg = "Не администратор канала не может выполнить эту операцию"
		}
	}

	_, _ = B.Edit(c.Message, msg)
}

func pingCmdCtr(m *tb.Message) {
	_, _ = B.Send(m.Chat, "pong")
	zap.S().Debugw(
		"pong",
		"telegram msg", m,
	)
}

func helpCmdCtr(m *tb.Message) {
	message := `
Команды:

/sub подписаться

/unsub  Отмена
订阅
/list список подписок

/set настроить подписку

/check проверить текущую подписку

/setfeedtag проверить тег подписки

/setinterval установить интервал

/activeall активировать все подписки

/pauseall приостановить все подписки

/help помощь

/import импорт OPML

/export экспорт OPML

/unsuball Отмена
所有订阅
详细使用方法请看：https://andhrimnir.albi.io/albi/flowerss-bot
`

	_, _ = B.Send(m.Chat, message)
}

func versionCmdCtr(m *tb.Message) {
	_, _ = B.Send(m.Chat, config.AppVersionInfo())
}

func importCmdCtr(m *tb.Message) {
	message := `Отправьте файл OPML реплаем,

Если вам нужно импортировать OPML для канала, укажите идентификатор канала при отправке файла, например @telegram.

`
	_, _ = B.Send(m.Chat, message)
}

func setFeedTagCmdCtr(m *tb.Message) {
	mention := GetMentionFromMessage(m)
	args := strings.Split(m.Payload, " ")

	if len(args) < 1 {
		B.Send(m.Chat, "/setfeedtag [sub id] [tag1] [tag2] Установить тег подписки (установить до трех тегов, разделенных пробелами)")
		return
	}

	var subID int
	var err error
	if mention == "" {
		// 截短参数
		if len(args) > 4 {
			args = args[:4]
		}
		subID, err = strconv.Atoi(args[0])
		if err != nil {
			B.Send(m.Chat, "Пожалуйста, введите правильный идентификатор подписки!")
			return
		}
	} else {
		if len(args) > 5 {
			args = args[:5]
		}
		subID, err = strconv.Atoi(args[1])
		if err != nil {
			B.Send(m.Chat, "Пожалуйста, введите правильный идентификатор подписки!")
			return
		}
	}

	sub, err := model.GetSubscribeByID(subID)
	if err != nil || sub == nil {
		B.Send(m.Chat, "Пожалуйста, введите правильный идентификатор подписки!")
		return
	}

	if !checkPermit(int64(m.Sender.ID), sub.UserID) {
		B.Send(m.Chat, "Доступ запрещен!")
		return
	}

	if mention == "" {
		err = sub.SetTag(args[1:])
	} else {
		err = sub.SetTag(args[2:])
	}

	if err != nil {
		B.Send(m.Chat, "订阅Настройки ярлыка失败!")
		return
	}
	B.Send(m.Chat, "订阅Настройки ярлыка成功!")
}

func setIntervalCmdCtr(m *tb.Message) {

	args := strings.Split(m.Payload, " ")

	if len(args) < 1 {
		_, _ = B.Send(m.Chat, "/setinterval [интервал] [sub id] Установить частоту обновления подписки (можно установить несколько дополнительных идентификаторов, разделенных пробелами)")
		return
	}

	interval, err := strconv.Atoi(args[0])
	if interval <= 0 || err != nil {
		_, _ = B.Send(m.Chat, "Пожалуйста, введите правильную частоту сканирования")
		return
	}

	for _, id := range args[1:] {

		subID, err := strconv.Atoi(id)
		if err != nil {
			_, _ = B.Send(m.Chat, "Пожалуйста, введите правильный идентификатор подписки!")
			return
		}

		sub, err := model.GetSubscribeByID(subID)

		if err != nil || sub == nil {
			_, _ = B.Send(m.Chat, "Пожалуйста, введите правильный идентификатор подписки!")
			return
		}

		if !checkPermit(int64(m.Sender.ID), sub.UserID) {
			_, _ = B.Send(m.Chat, "Доступ запрещен!")
			return
		}

		_ = sub.SetInterval(interval)

	}
	_, _ = B.Send(m.Chat, "Частота сканирования установлена успешно!")

	return
}

func activeAllCmdCtr(m *tb.Message) {
	mention := GetMentionFromMessage(m)
	if mention != "" {
		channelChat, err := B.ChatByID(mention)
		if err != nil {
			_, _ = B.Send(m.Chat, "error")
			return
		}
		adminList, err := B.AdminsOf(channelChat)
		if err != nil {
			_, _ = B.Send(m.Chat, "error")
			return
		}

		senderIsAdmin := false
		for _, admin := range adminList {
			if m.Sender.ID == admin.User.ID {
				senderIsAdmin = true
			}
		}

		if !senderIsAdmin {
			_, _ = B.Send(m.Chat, fmt.Sprintf("Не администратор канала не может выполнить эту операцию"))
			return
		}

		_ = model.ActiveSourcesByUserID(channelChat.ID)
		message := fmt.Sprintf("Все подписки на канал [%s] (https://t.me/%s) активированы", channelChat.Title, channelChat.Username)

		_, _ = B.Send(m.Chat, message, &tb.SendOptions{
			DisableWebPagePreview: true,
			ParseMode:             tb.ModeMarkdown,
		})

	} else {
		_ = model.ActiveSourcesByUserID(m.Chat.ID)
		message := "Подписки все включены"

		_, _ = B.Send(m.Chat, message, &tb.SendOptions{
			DisableWebPagePreview: true,
			ParseMode:             tb.ModeMarkdown,
		})
	}

}

func pauseAllCmdCtr(m *tb.Message) {
	mention := GetMentionFromMessage(m)
	if mention != "" {
		channelChat, err := B.ChatByID(mention)
		if err != nil {
			_, _ = B.Send(m.Chat, "error")
			return
		}
		adminList, err := B.AdminsOf(channelChat)
		if err != nil {
			_, _ = B.Send(m.Chat, "error")
			return
		}

		senderIsAdmin := false
		for _, admin := range adminList {
			if m.Sender.ID == admin.User.ID {
				senderIsAdmin = true
			}
		}

		if !senderIsAdmin {
			_, _ = B.Send(m.Chat, fmt.Sprintf("Не администратор канала не может выполнить эту операцию"))
			return
		}

		_ = model.PauseSourcesByUserID(channelChat.ID)
		message := fmt.Sprintf("Подписка на канал [%s] (https://t.me/%s) приостановлена", channelChat.Title, channelChat.Username)

		_, _ = B.Send(m.Chat, message, &tb.SendOptions{
			DisableWebPagePreview: true,
			ParseMode:             tb.ModeMarkdown,
		})

	} else {
		_ = model.PauseSourcesByUserID(m.Chat.ID)
		message := "Все подписки приостановлены"

		_, _ = B.Send(m.Chat, message, &tb.SendOptions{
			DisableWebPagePreview: true,
			ParseMode:             tb.ModeMarkdown,
		})
	}

}

func textCtr(m *tb.Message) {
	switch UserState[m.Chat.ID] {
	case fsm.UnSub:
		{
			str := strings.Split(m.Text, " ")

			if len(str) < 2 && (strings.HasPrefix(str[0], "[") && strings.HasSuffix(str[0], "]")) {
				_, _ = B.Send(m.Chat, "Пожалуйста, выберите правильную инструкцию!")
			} else {

				var sourceID uint
				if _, err := fmt.Sscanf(str[0], "[%d]", &sourceID); err != nil {
					_, _ = B.Send(m.Chat, "Пожалуйста, выберите правильную инструкцию!")
					return
				}

				source, err := model.GetSourceById(sourceID)

				if err != nil {
					_, _ = B.Send(m.Chat, "Пожалуйста, выберите правильную инструкцию!")
					return
				}

				err = model.UnsubByUserIDAndSource(m.Chat.ID, source)

				if err != nil {
					_, _ = B.Send(m.Chat, "Пожалуйста, выберите правильную инструкцию!")
					return
				}

				_, _ = B.Send(
					m.Chat,
					fmt.Sprintf("[%s] (%s) успешно отписались", source.Title, source.Link),
					&tb.SendOptions{
						ParseMode: tb.ModeMarkdown,
					}, &tb.ReplyMarkup{
						ReplyKeyboardRemove: true,
					},
				)
				UserState[m.Chat.ID] = fsm.None
				return
			}
		}

	case fsm.Sub:
		{
			url := strings.Split(m.Text, " ")
			if !CheckURL(url[0]) {
				_, _ = B.Send(m.Chat, "Пожалуйста, отправьте реплаем правильный URL", &tb.ReplyMarkup{ForceReply: true})
				return
			}

			registFeed(m.Chat, url[0])
			UserState[m.Chat.ID] = fsm.None
		}
	case fsm.SetSubTag:
		{
			return
		}
	case fsm.Set:
		{
			str := strings.Split(m.Text, " ")
			url := str[len(str)-1]
			if len(str) != 2 && !CheckURL(url) {
				_, _ = B.Send(m.Chat, "Пожалуйста, выберите правильную инструкцию!")
			} else {
				source, err := model.GetSourceByUrl(url)

				if err != nil {
					_, _ = B.Send(m.Chat, "Пожалуйста, выберите правильную инструкцию!")
					return
				}
				sub, err := model.GetSubscribeByUserIDAndSourceID(m.Chat.ID, source.ID)
				if err != nil {
					_, _ = B.Send(m.Chat, "Пожалуйста, выберите правильную инструкцию!")
					return
				}
				t := template.New("setting template")
				_, _ = t.Parse(feedSettingTmpl)

				toggleNoticeKey := tb.InlineButton{
					Unique: "set_toggle_notice_btn",
					Text:   "Включите уведомления",
				}
				if sub.EnableNotification == 1 {
					toggleNoticeKey.Text = "Закрыть уведомление"
				}

				toggleTelegraphKey := tb.InlineButton{
					Unique: "set_toggle_telegraph_btn",
					Text:   "Включите транскодирование Telegraph",
				}
				if sub.EnableTelegraph == 1 {
					toggleTelegraphKey.Text = "Отключить перекодировку Telegraph"
				}

				toggleEnabledKey := tb.InlineButton{
					Unique: "set_toggle_update_btn",
					Text:   "Приостановить обновление",
				}

				if source.ErrorCount >= config.ErrorThreshold {
					toggleEnabledKey.Text = "Перезапустить обновление"
				}

				feedSettingKeys := [][]tb.InlineButton{
					[]tb.InlineButton{
						toggleEnabledKey,
						toggleNoticeKey,
						toggleTelegraphKey,
					},
				}

				text := new(bytes.Buffer)

				_ = t.Execute(text, map[string]interface{}{"source": source, "sub": sub, "Count": config.ErrorThreshold})

				// send null message to remove old keyboard
				delKeyMessage, err := B.Send(m.Chat, "processing", &tb.ReplyMarkup{ReplyKeyboardRemove: true})
				err = B.Delete(delKeyMessage)

				_, _ = B.Send(
					m.Chat,
					text.String(),
					&tb.SendOptions{
						ParseMode: tb.ModeHTML,
					}, &tb.ReplyMarkup{
						InlineKeyboard: feedSettingKeys,
					},
				)
				UserState[m.Chat.ID] = fsm.None
			}
		}
	}
}

// docCtr Document handler
func docCtr(m *tb.Message) {
	if m.FromGroup() {
		if !userIsAdminOfGroup(m.Sender.ID, m.Chat) {
			return
		}
	}

	if m.FromChannel() {
		if !UserIsAdminChannel(m.ID, m.Chat) {
			return
		}
	}

	url, _ := B.FileURLByID(m.Document.FileID)
	if !strings.HasSuffix(url, ".opml") {
		B.Send(m.Chat, "Если вам нужно импортировать подписки, отправьте правильный файл OPML.")
		return
	}

	opml, err := GetOPMLByURL(url)
	if err != nil {
		if err.Error() == "fetch opml file error" {
			_, _ = B.Send(m.Chat,
				"Не удалось загрузить файл OPML. Проверьте, может ли бот-сервер нормально подключаться к серверу Telegram, или попробуйте импортировать его позже. Код ошибки 02")

		} else {
			_, _ = B.Send(
				m.Chat,
				fmt.Sprintf(
					"Если вам нужно импортировать подписки, отправьте правильный файл OPML. 错误代码 01，doc mimetype: %s",
					m.Document.MIME),
			)
		}
		return
	}

	userID := m.Chat.ID
	mention := GetMentionFromMessage(m)
	if mention != "" {
		// import for channel
		channelChat, err := B.ChatByID(mention)
		if err != nil {
			_, _ = B.Send(m.Chat, "Ошибка получения информации о канале. Проверьте правильность идентификатора канала.")
			return
		}

		if !checkPermitOfChat(int64(m.Sender.ID), channelChat) {
			_, _ = B.Send(m.Chat, fmt.Sprintf("Не администратор канала не может выполнить эту операцию"))
			return
		}

		userID = channelChat.ID
	}

	message, _ := B.Send(m.Chat, "Обработка, подождите ...")
	outlines, _ := opml.GetFlattenOutlines()
	var failImportList []Outline
	var successImportList []Outline

	for _, outline := range outlines {
		source, err := model.FindOrNewSourceByUrl(outline.XMLURL)
		if err != nil {
			failImportList = append(failImportList, outline)
			continue
		}
		err = model.RegistFeed(userID, source.ID)
		if err != nil {
			failImportList = append(failImportList, outline)
			continue
		}
		zap.S().Infof("%d subscribe [%d]%s %s", m.Chat.ID, source.ID, source.Title, source.Link)
		successImportList = append(successImportList, outline)
	}

	importReport := fmt.Sprintf("<b>Успешный импорт:% d, сбой импорта:% d </b>", len(successImportList), len(failImportList))
	if len(successImportList) != 0 {
		successReport := "\n\n<b>Следующие каналы были успешно импортированы:</b>"
		for i, line := range successImportList {
			if line.Text != "" {
				successReport += fmt.Sprintf("\n[%d] <a href=\"%s\">%s</a>", i+1, line.XMLURL, line.Text)
			} else {
				successReport += fmt.Sprintf("\n[%d] %s", i+1, line.XMLURL)
			}
		}
		importReport += successReport
	}

	if len(failImportList) != 0 {
		failReport := "\n\n<b>Не удалось импортировать следующие каналы:</b>"
		for i, line := range failImportList {
			if line.Text != "" {
				failReport += fmt.Sprintf("\n[%d] <a href=\"%s\">%s</a>", i+1, line.XMLURL, line.Text)
			} else {
				failReport += fmt.Sprintf("\n[%d] %s", i+1, line.XMLURL)
			}
		}
		importReport += failReport
	}

	_, _ = B.Edit(message, importReport, &tb.SendOptions{
		DisableWebPagePreview: true,
		ParseMode:             tb.ModeHTML,
	})
}

func errorCtr(m *tb.Message, errMsg string) {
	_, _ = B.Send(m.Chat, errMsg)
}
